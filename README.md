# **Generate Arabic text on images**
Given text files in the `Input/` direcotry, generate images with random backgrounds, blur values and distortion values in the `Output/` directory and save the original text files in a `.gt.txt` format.

This project is used to re-train Tesseract for the Arabic language.

### **Input**
The sample text files obtained from
- [Arabic poems dataset](https://www.kaggle.com/fahd09/arabic-poetry-dataset-478-2017)
- [Yarmouk dataset](https://drive.google.com/drive/u/0/folders/0B4Kx3iMuktgsdC12Ui1neklnMzQ)
- [ATSD dataset](http://www.mohamedaly.info/datasets/astd)
- [KAFD dataset](http://kafd.ideas2serve.net/)
- [Randomly selected wikipedia articles](https://ar.wikipedia.org/wiki/%D8%B9%D9%84%D9%85_%D8%A7%D9%84%D9%86%D9%81%D8%B3)
***

### **Output**
* Images containing Arabic text read from the `Input/` directory
* Ground truth text files containing Arabic text read from the `Input/` directory
* Images and GT files have the same name as the files in the `Input/` directory
* Filenames format `{input_directory_name}_{counter}.{extension}`
***

### **What's new**
- Added support for multiple directories
- Reformatted filenames
- Now using .tif extension for images instead of .png (recommended by Tesseract)
***

### **Important Notes**
In order to display the Arabic letters on the images
* Use the font `Sahel.ttf`

  - Download the font from [here](https://github.com/rastikerdar/sahel-font/blob/master/dist/Sahel.ttf)
  - Make a new directory called `ar` in the fonts directory in the TRDG installation path </br>
    Directory path on Ubuntu: `~/anaconda3/envs/tf1/lib/python3.7/site-packages/trdg/fonts/`
  - Copy `Sahel.ttf` to the newly created `ar` directory

* Add the following lines in the `from_strings.py` file in the `__init__` method

So, the `__init__` method will look like this:
```Python
...
        self.count = count

        ### Add these two lines ###
        from bidi.algorithm import get_display
        strings = [get_display(arabic_reshaper.reshape(s)) for s in strings]

        self.strings = strings
        self.fonts = fonts
        if len(fonts) == 0:
            self.fonts = load_fonts(language)
...
```

* You can also just replace the `from_strings.py` file found in `<python directory>/site-packages/trdg/generators` with the one in this repository.

* bidi-algorithm is required in order for the script to run. </br>

***

### **Run the generator**
To run with the default aparemters, simply run the command

```shell
bash ./auto_run.sh
```

Otherwise, please refer to the [TRDG official GitHub repository](https://github.com/Belval/TextRecognitionDataGenerator) for more information.
***

**Special thanks to the team authorizing the KAFD dataset for granting me access.**