'''
* Read text from files
* Put text on images with various backgrounds and various blur ratios
* Save results

 * Fonts used:
    ** Sahel.ttf
    ** Amiri-Regular.ttf
    ** NotoSansArabic-Regular.ttf

# Thabit -> 0
# Al-Mohanad -> 0
# Abo Salman -> 1
# Samim -> 1
'''

import os
import numpy as np

from trdg.generators import GeneratorFromStrings

# For debugging a new font
DEBUG = False

class GenerateData:
    '''Generate text for OCR'''
    def __init__(self, in_dir='Input/', out_dir='Output_2/' if DEBUG else 'Output/'):
        '''Initialize variables'''
        self.out_dir = out_dir    # output directory
        self.in_dir = in_dir      # input directory
        self.counter = 0

    def read_text_files(self):
        '''
        Read images from a given path.

        Returns:
            images (list of numpy arrays): list of images found in path
        '''
        print('[INFO] Reading files...')
        text_files = {}

        # Scan the directory for text files
        for root, _, file_names in os.walk(self.in_dir):
            if DEBUG:
                num_gen = 3
            else:
                num_gen = len(file_names)

            for file_name in file_names[:num_gen]:
                if file_name.endswith(('txt')):
                    file_path = root + '/' + file_name
                    with open(file_path, 'r') as f:
                        text = f.readline()
                        text_files[file_path] = text

        # The fine names are saved in order to generate the ground truth text and image files
        # with the same name as the original (required for Tesseract)
        print(f'[DEBUG] Found {len(text_files)} files in folder ./{self.in_dir}')
        return text_files


    def generate_img_text(self, text):
        '''
        Generate images with various backgrounds with the given text

        Args:
            text (str): text

        Returns:
            img (PIL image): image with text
        '''
        print('[DEBUG] Generating image...')

        # Random backgorund type -- either 0 or 1
        bg_type = np.random.randint(2)

        # Random distorsion type -- either 0 or 3
        # dist_type = np.random.choice([0, 3])

        # Generate images from the given text
        generator = GeneratorFromStrings(
            [text],                            # text
            blur=1,                            # maximum amount of blur to be applied
            random_blur=True,                  # aply random amount of blur on each image
            language='ar',                     # use the Arabic language
            count=1,                           # generate one image per input
            background_type=bg_type,           # set a random background image (0 or 1)
            # distorsion_type=dist_type,         # set a random distorsion type (0 or 3)
            size=128                           # size of the output image (Heightx128)
        )

        for img, _ in generator:
            return img


    def save_generated_img(self, file_path, img):
        '''
        Save image in a specified file path

        Args:
            file_path (str): path of the *input* file
            img (PIL image): image with text
        '''

        ### File format: {input_dir}_{str(counter).zfill(5)}.tiff

        file_path = file_path.split('/')[1].split('_')[0]

        if DEBUG:
            out_file_name = file_path.lower() + '_' + str(self.counter).zfill(5)
        else:
            out_file_name = file_path.lower() + '_' + str(self.counter).zfill(5) + '_7'

        out_path = f'{self.out_dir}/{out_file_name}.tif'    # output path

        # Save image
        img.save(out_path)

        print(f'[DEBUG] Saved image in {out_path}')


    def save_generated_text(self, file_path, text):
        '''
        Save text in a specified file path

        Args:
            file_path (str): path of the *input* file
            text (str): text from the input file
                        added just to keep the consistency of parameters
                        with `save_generated_img`
        '''
        ### File format: {input_dir}_{str(counter).zfill(5)}.gt.txt
        file_path = file_path.split('/')[1].split('_')[0]

        if DEBUG:
            out_file_name = file_path.lower() + '_' + str(self.counter).zfill(5)
        else:
            out_file_name = file_path.lower() + '_' + str(self.counter).zfill(5) + '_7'

        out_path = f'{self.out_dir}/{out_file_name}.gt.txt'    # output path

        # Write the text in the new GT file
        with open(out_path, 'w') as f:
            f.write(text)

        print(f'[DEBUG] Saved text file in {out_path}')


    def pipe(self, file_path, text):
        '''
        Pipeline for generating text on image

        Args:
            file_path (str): path of the *input* file
            text (str): text in file_path
        '''
        # Generate the image with text
        img = self.generate_img_text(text)

        # Save the generated image
        self.save_generated_img(file_path, img)

        # Save the text in the .gt.txt format
        if not DEBUG:
            self.save_generated_text(file_path, text)

        self.counter += 1


def main():
    '''Main'''
    Jenny = GenerateData()

    # Read all the files from the input path
    text_files = Jenny.read_text_files()

    # Generate and save images and text files in the output path
    for file_name, text in text_files.items():
        print(f'[DEBUG] File name: {file_name}')
        try:
            Jenny.pipe(file_name, text)
        except ValueError:
            continue

    # DONE ^ ^
    print('DONE uwu')


if __name__ == '__main__':
    usr_in = input(f'Debug is {DEBUG}. Also counter is _7. Continue? ')

    if usr_in == 'y':
        main()
    else:
        print('kthxbye')
